<?php
/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 4/9/16
 * Time: 9:51 PM
 */
if(!isset($_GET['verifcode'])){
header('HTTP/1.0 403 Forbidden');
die();
}else{

    (defined('__DIR__'))? define('DOCUMENT_ROOT','__DIR__'): define('DOCUMENT_ROOT',dirname(__FILE__));

    require DOCUMENT_ROOT.'/__autoload.php';


    $db=new database();
    $v_code=htmlspecialchars($_GET['verifcode']);
    if($db->countTable('users','user_id','user_v_code','verified')==1){
        echo 'User already verified';
        exit();
    }
    $id=$db->fetchSingle('users','user_id','user_v_code',$v_code);
    if($db->countTable('users','user_id','user_v_code',$v_code)==1){
        $upd=$db->innerupdate('users',array(1),array('user_account_status'),'user_v_code',$v_code);
        if($upd){
            echo "Verfication successful";
            $db->innerupdate('users',array('verified'),array('user_v_code'),'user_id',$id);
        }else{
            echo "Verification Failed";
        }
    }else{
        echo "Invalid verification code";
    }



}
