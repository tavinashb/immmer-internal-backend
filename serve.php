<?php
/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 4/2/16
 * Time: 12:57 PM
 */
(defined('__DIR__'))? define('DOCUMENT_ROOT','__DIR__'): define('DOCUMENT_ROOT',dirname(__FILE__));

    require DOCUMENT_ROOT . '/__autoload.php';
    if(isset($authorization)) {
        $oauth->access_token = $db->sanitizer($access_token);
        $oauth->allowAccessToken();

        $json->json = array(errorJson('No data selected', $tools->httpStatus(501, null)));
        if (isset($tools->url()[0])) {
            if ($tools->url()[0] === 'event') {
                $cevent->access_token=$access_token;
                $json->json=$cevent->compileEvent();
            }


            if ($tools->url()[0] === 'user') {
                $cusr->access_token=$access_token;
                $json->json=$cusr->compileUsers();
            }
        }

        if(empty($json->json)) $json->json=errorJson('Not found',$tools->httpStatus(404));
        echo $json->return_json();

    }