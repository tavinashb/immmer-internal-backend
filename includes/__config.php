<?php
/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 4/1/16
 * Time: 10:50 PM
 */

#########   CONNECTION FILE TO DATABASE    #########
#########   DRIVER IN USE (MYSQL PDO)      #########
########    APP => IMMMMER API (PHP)       #########
########    DATABASE SCHEMA ON 'includes/schema  ###


define('DBNAME','immmer_api');//database name
define('HOST','localhost'); //localhost
define('DBUSER','root');//database username
define('DBPASS','bitnami'); //database password
define('GOOGLE_API_ADDRESS','http://maps.google.com/maps/api/geocode/json?sensor=false&address=');
define('GOOGLE_API_KEY','');

define('INCLUDE_SCRIPT',true);//do not remove this unless you really know what you are doing


define('LOG_ERROR',true); //turn on error logging
define('ERROR_PATH',DOCUMENT_ROOT.'/includes/error/error.log');//error log location
define('MULTIMEDIA_LOCATION',DOCUMENT_ROOT.'/style/multimedia');//multimedia location
define('DATABASE_SCHEMA',DOCUMENT_ROOT.'/includes/schema/database_schema.sql');//database schema location
define('DISPLAY_SCREEN_ERROR',false);//turn off to stop error display on the screen

