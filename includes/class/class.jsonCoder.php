<?php

/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 4/1/16
 * Time: 11:25 PM
 */
class jsonCoder
{

    public $json;
    public $json_option;
    public $json_encoded;
    public $json_titled=array();


    public function __construct()
    {
        $this->json_option=array(
            0=>JSON_NUMERIC_CHECK,//check the numeric and assign number
            1=> JSON_FORCE_OBJECT,//force json as an object
            2=> JSON_UNESCAPED_UNICODE,//keeps unicode character
        );

    }

    public function encode_json(){
        return utf8_encode($this->json);
    }
    public function valid_json(){
        json_decode($this->json)!==json_last_error()? :true;
    }


    public function return_json(){
        json_encode($this->json)? $this->json_encoded=json_encode($this->json,$this->json_option[0]): false;
        if(json_last_error()===JSON_ERROR_UTF8){//check if utf8
            return false;
        }else{
           return $this->json_encoded;

        }
    }

}
