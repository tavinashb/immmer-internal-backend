<?php

/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 4/1/16
 * Time: 10:43 PM
 */
class oauth
{
    public $access_token;
    private $grant_array;
    public $grant_type;//password, refresh_token, registration
    private $error;
    public $json;
    public $method;
    public $error_description;
    public $error_details;
    public $error_reason;
    public $error_code;
    private $database_class;
    private $password_hash;
    public $malformed;


    public function __construct($grant, $method)
    {
        $this->password_hash = new PasswordHash(8, TRUE);
        $this->method = $method;
        $this->grant_array = $this->grants();
        $this->grant_type = $grant;
        $this->database_class = new database();

        $this->json = new jsonCoder();
        if (!in_array($this->grant_type, $this->grant_array)) {

            $this->json->json['error'] = $this->error_param(
                $this->error_details,
                htmlspecialchars($_SERVER['PHP_SELF']),
                filter_var_array($method),
                htmlspecialchars($this->method),
                'Please provide a valid grant_type, supported types are : password, registration or refresh_token', 'invalid_grant', 401);
            echo $this->json->return_json();
            die();
        }
    }


    public function grants()
    {
        return array(
            'facebook_login_grant',
            'facebook_registration_grant',
            'refresh_token',
            'access_token'
        );
    }


    public function generate($length = 10)
    {//generator random code
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[mt_rand(0, $charactersLength - 1)];
        }
        return md5($randomString);
    }


    public function error_param(
        $error_detail,
        $url,
        $query_string,
        $method,
        $error_description,
        $error_reason,
        $error_code)
    {

        ob_start();
        header('HTTP/1.1 '.$error_code.' '.$error_reason);
        return array(
            'detail' =>  $error_reason,
            'request' => array(
                'url' => $url,
                'query_string' => $query_string,
                'method' => $method),
            'description' => $error_description,
            'status' => 'error',
            'status_code' => $error_code
        );
    }

    public function success_param(
        $success_detail,
        $url,
        $query_string,
        $method,
        $success_description,
        $success_reason,
        $success_code)
    {

        ob_start();
        header('HTTP/1.1 '.$success_code.' '.$success_reason);
        return array(
            'detail' => $success_reason,
            'request' => array(
                'url' => $url,
                'query_string' => $query_string,
                'method' => $method),
            'description' => $success_description,
            'status' => 'success',
            'status_code' => $success_code
        );
    }


    public function registration_auth($data)
    {
        $error = $firstname = $lastname = $email = $username = $password = $sex = $insert = '';

        if (empty($this->database_class->sanitizer(@$data['firstname'], true))) {
            $error .= 'firstname is empty, ';
        } elseif (!preg_match("~([A-Za-z])~", $this->database_class->sanitizer(@$data['firstname'], true))) {
            $error .= 'firstname contains numbers or special character, ';
        } else {
            $firstname = $this->database_class->sanitizer(@$data['firstname'], true);//firstname
        }


        if (empty($this->database_class->sanitizer(@$data['lastname']))) {
            $error .= 'lastname is empty, ';
        } elseif (!preg_match("~([A-Za-z])~", $this->database_class->sanitizer(@$data['lastname']))) {
            $error .= 'lastname contains numbers or special character, ';
        } else {
            $lastname = $this->database_class->sanitizer(@$data['lastname'], true);//lastname
        }

    //    if (empty($this->database_class->sanitizer(@$data['email']))) {
    //        $error .= 'email is empty, ';
    //    }  elseif (!preg_match("~([A-Za-z0-9]@[A-Za-z0-9].[A-Za-z0-9])~", $this->database_class->sanitizer(@$data['email']))) {
    //        $error .= 'Invalid email format';
    //    } elseif ($this->database_class->countTable('users', 'user_id', 'user_email', $this->database_class->sanitizer(@$data['email'])) >= 1) {
    //        $error .= 'email already exist, ';
    //    } else {
    //        $email = $this->database_class->sanitizer(@$data['email'], true);//email
    //    }

        $email=htmlspecialchars($_REQUEST['email']);

        $data['password']=$this->generate(2);
        if (empty($this->database_class->sanitizer(@$data['password'], true))) {
            $error = 'Password must not be empty, ';
        } elseif (strlen($this->database_class->sanitizer(@$data['password'], true)) < 5) {
            $error = 'Password must be greater than 5, ';
        } else {


            $password = $this->password_hash->HashPassword(
                $this->database_class->sanitizer(@$data['password'], true)
            );//password hasher
        }


        if (empty($this->database_class->sanitizer(@$data['username']))) {
            $error = 'username is empty';
        } elseif (strlen($this->database_class->sanitizer(@$data['username'])) < 3) {
            $error = 'username must be greater than 3, ';
        } elseif ($this->database_class->countTable('users', 'user_id', 'user_nickname', $this->database_class->sanitizer(@$data['username'])) >= 1) {
            $error = 'username already exist, ';
        } else {
            $username = $this->database_class->sanitizer(@$data['username'], true);//username
        }

        if (strlen($this->database_class->sanitizer(@$data['sex'], true)) >= 4) {
            $sex = $this->database_class->sanitizer(@$data['sex'], true);
        }

        $subscription = 1;//subscription
        $permission = 1;//permission
        $data_joined = date('Y-m-d H:i:s');//datetime
        $account_status = 1;//account status (verification needed

        $fb_id=@$data['fb_id'];
        $fb_photo=@$data['photo'];
        if (
            $firstname !== ''
            && $lastname !== ''
            && $username !== ''
            && $password !== ''
        ) {
            $coder=$this->generate(50);
            //$host=$_SERVER['HTTP_HOST'];
            //$to=$email;
            //$subject='Verify your account';
            //$message="<html><head><title>Immmer registration</title></head><body style='background:#cccccc; border-radius:4px;'>";
            //$message.="<div style='border-radius:5px; box-shadow:0px 0px 9px rgba(0,0,0,0.4); border:1px solid #ccc; padding:5%;'>";
            //$message.="<h3 style='color:#FF8080; text-align:center; text-transform: uppercase; text-decoration:underline;'>Immmer Registration</h3>";
            //$message.="<p style='font-weight:bold; float:left;'>Dear ".$firstname.",</p>"."\n\r";
            //$message.="<div style='clear:both'></div>";
            //$message.="<p style='font-weight:bold;'>";
            //$message.="Thank you for choosing IMMMER \n\r";
            //$message.="</p>";
            //$message.="<p style='font-weight:bold;'>";
            //$message.="Verification link: ";
            //$message.="</p>";
            //$message.="<p style='color:#666; text-align:justify;'>";
            //$message.='http://'.$host."/verify.php?verifcode=".$coder."\n\r";
            //$message.="</p> \n\r";
            //$message.="<a style='text-decoration:none; color:white;' href='http://".$host."/verify.php?verifcode=".$coder."'><button style='width:100%; background:linear-gradient(to bottom,#FF8080,orange); font-weight:bold; color:white; padding: 1%; border-radius:10px;'>Active Now!</button></a>";
            //$message.="</div></body></html>";
            //$plain= "Dear ".$firstname.", \n\r";
            //$plain.="Thank you for choosing IMMMER \n\r";
            //$plain.="Verification link: \n\r";
            //$plain.='http://'.$host."/verify.php?verifcode=".$coder."\n\r";
            //$mailer=new mailer();
            //$mailer->to=$to;
            //$mailer->to_name=$firstname;
            //$mailer->subject=$subject;
            //$mailer->messageHtml=$message;
            //$mailer->messagePlain=$plain;
            //$mail=$mailer->mailNow();

           // if($mail===true){
                $insert = $this->database_class->innerInsert(
                    'users',
                    array(
                        $coder,
                        $firstname,
                        $lastname,
                        $email,
                        $password,
                        $username,
                        $subscription,
                        $permission,
                        $data_joined,
                        $sex,
                        $account_status,
                        $fb_id,
                        $fb_photo,
                        1

                    ),

                    array(
                        'user_v_code',
                        'user_firstname',
                        'user_lastname',
                        'user_email',
                        'user_password',
                        'user_nickname',
                        'user_subscription',
                        'user_permission_id',
                        'user_date_joined',
                        'user_sex',
                        'user_account_status',
                        'user_fb_id',
                        'profile_photo',
                        'user_location'
                    )
                );

            //}else{
            //    $error="unable to send verification email, registration failed";
            //}

        }

        if ($insert==true) {
            $success = 'registration success, Check your email for verification code';

            unset($data['password']);
            $this->json->json['registration'] = array($this->success_param(
                null,
                htmlspecialchars($_SERVER['PHP_SELF']),
                filter_var_array(@$data),
                htmlspecialchars($this->method),
                $success,
                'Created',
                201
            ));
            echo $this->json->return_json();
            die();

        } else {

            $this->json->json['registration'] = array($this->error_param(
                $this->error_details,
                htmlspecialchars($_SERVER['PHP_SELF']),
                filter_var_array(@$data),
                htmlspecialchars($this->method),
                $error,
                'Not implemented',
                501
            ));
            echo $this->json->return_json();
            die();


        }
    }


    public function refresh_token($data)
    {

        $insert = '';
        $client_id = @$data['client_id'];
        $refresh_token = @$data['refresh_token'];
        $access_token = @$data['access_token'];
        if (strlen($client_id) > 2 && strlen($refresh_token) > 30 && strlen($access_token) > 30 &&
            $this->database_class
                ->countTable('access_token', 'client_id', 'access_token',
                    $access_token, 'client_id', $client_id) >= 1 &&
            $this->database_class
                ->countTable('refresh_token', 'client_id', 'refresh_token',
                    $refresh_token, 'client_id', $client_id) >= 1 &&
            $this->database_class->fetchSingle('users','user_account_status','user_id',
                $this->database_class->fetchSingle('refresh_token','user_id','refresh_token',
                    $refresh_token,'client_id',$client_id))==1
        ) {
            for ($i = 0; $i < 100; $i++) {
                $accesstoken = $this->generate(35);
                $refreshtoken = $this->generate(38);
                $clientid = $this->generate(5);
                $insert .= $this->database_class->innerupdate('access_token',
                    array(
                        $accesstoken,
                        date("Y-m-d H:i:s", strtotime('+10 minutes'))//+(360)
                    ),
                    array(
                        'access_token',
                        'expires'
                    ), 'access_token', $access_token, 'client_id', $client_id);
                $insert .= $this->database_class->innerupdate('refresh_token',
                    array(
                        $refreshtoken,
                    ),
                    array(
                        'refresh_token',
                    ), 'refresh_token', $refresh_token, 'client_id', $client_id);
                if ($insert) {
                    $this->json->json['token'] = array(array(
                        'access_token' => $accesstoken,
                        'token_type' => 'refresh_token',
                        'expires_in' => 360,
                        'refresh_token' => $refreshtoken,
                        'client_id' => $client_id,
                        'status' =>'success'
                    ));
                    echo $this->json->return_json();
                    die();
                } else {
                    $this->json->json['token'] = array($this->error_param(
                        $this->error_details,
                        htmlspecialchars($_SERVER['PHP_SELF']),
                        filter_var_array(@$data),
                        htmlspecialchars($this->method),
                        'either refresh token has expired, revoked or no data specified',
                        'unauthorized',
                        401
                    ));
                    echo $this->json->return_json();
                    die();
                }
            }
        } else {

            $this->json->json['token'] = array($this->error_param(
                $this->error_details,
                htmlspecialchars($_SERVER['PHP_SELF']),
                filter_var_array(@$data),
                htmlspecialchars($this->method),
                'either refresh token has expired, revoked or no data specified',
                'unauthorized',
                401
            ));
            echo $this->json->return_json();
            die();
        }

    }

    public function password_auth($data)
    {
        $error = '';
        $fid = $this->database_class->sanitizer(@$data['fb_id'], true);
        $password = $this->database_class->sanitizer(@$data['immmer_app']);
        $platform=$this->database_class->sanitizer(@$data['platform']);
        if (in_array(@$data['password'], filter_var_array($data,FILTER_SANITIZE_STRING))) {
            unset($data['immmer_app']);
        }



        if (strlen($fid) >= 1 && strlen($password) >= 3 ){
            if ($this->database_class->countTable('users', 'user_id', 'user_fb_id', $fid) >= 1 &&
                $this->database_class->countTable('settings', 'setting_id', 'name',$platform)==1) {//check if platform exists
                if ($this->password_hash->CheckPassword($password, $this->database_class->fetchSingle('settings', 'value', 'name', $platform))) {
                    $insert = '';
                    for ($i = 0; $i < 100; $i++) {
                        $accesstoken = $this->generate(35);
                        $refresh_token = $this->generate(38);
                        $client_id = substr($this->generate(5),0,5);
                        $user_id = $this->database_class->fetchSingle(
                            'users',
                            'user_id',
                            'user_fb_id',
                            $fid
                        );
                        if ($this->database_class->countTable('access_token', 'user_id', 'access_token', $accesstoken, 'client_id', $client_id) < 1 || $this->database_class->countTable('refresh_token', 'user_id', 'access_token', $accesstoken, 'client_id', $client_id) < 1) {
                            $insert .= $this->database_class->innerInsert('access_token',
                                array(
                                    $accesstoken,
                                    $client_id,
                                    $user_id,
                                    date("Y-m-d H:i:s", strtotime('+10 minutes'))//+(360)
                                ),
                                array(
                                    'access_token',
                                    'client_id',
                                    'user_id',
                                    'expires'
                                ));
                            $insert .= $this->database_class->innerInsert('refresh_token',
                                array(
                                    $refresh_token,
                                    $client_id,
                                    $user_id,
                                    date("Y-m-d H:i:s", strtotime('+24 days'))//+24 days expiry
                                ),
                                array(
                                    'refresh_token',
                                    'client_id',
                                    'user_id',
                                    'expires'
                                ));
                            if ($insert) {
                                $this->json->json['token'] = array(array(
                                    'access_token' => $accesstoken,
                                    'token_type' => 'access_token',
                                    'expires_in' => 360,
                                    'refresh_token' => $refresh_token,
                                    'client_id' => $client_id,
                                    'status' =>'success'
                                ));
                                echo $this->json->return_json();
                                die();
                            }
                        }
                    }

                } else {

                    $this->json->json['login'] = array($this->error_param(
                        $this->error_details,
                        htmlspecialchars($_SERVER['PHP_SELF']),
                        filter_var_array(@$data),
                        htmlspecialchars($this->method),
                        'invalid_credientials',
                        'unauthorized',
                        401
                    ));
                    echo $this->json->return_json();
                    die();
                }
            } else {

                $this->json->json['login'] = array($this->error_param(
                    $this->error_details,
                    htmlspecialchars($_SERVER['PHP_SELF']),
                    filter_var_array(@$data),
                    htmlspecialchars($this->method),
                    'invalid_credientials or account not activated',
                    'unauthorized',
                    401
                ));
                echo $this->json->return_json();
                die();
            }
        } else {

            $this->json->json['login'] = array($this->error_param(
                $this->error_details,
                htmlspecialchars($_SERVER['PHP_SELF']),
                filter_var_array(@$data),
                htmlspecialchars($this->method),
                'either username or password was less than 3 characters',
                'not implemented',
                501
            ));
            echo $this->json->return_json();
            die();
        }


    }

    public function allowAccessToken(){
        if(
        $this->database_class->countTable('access_token','client_id','access_token',
            $this->access_token)!==1 &&
        $this->database_class->fetchSingle('access_token','expires','access_token',
            $this->access_token)<date('Y-m-d H:i:s')){
            $this->json->json['token'] = array($this->error_param(
                $this->error_details,
                htmlspecialchars($_SERVER['PHP_SELF']),
                $this->access_token,
                htmlspecialchars($this->method),
                'invalid token',
                'bad_request',
                400
            ));
            echo $this->json->return_json();
            die();
        }
        }


}



