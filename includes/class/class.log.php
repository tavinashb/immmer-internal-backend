<?php

/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 4/1/16
 * Time: 10:53 PM
 */
class log
{
    private $error_path=ERROR_PATH;
    private $log_error=LOG_ERROR;
    private $screen_error=DISPLAY_SCREEN_ERROR;
    public $error;


    public function __construct()
    {


    }
    public function create_log(){
        if($this->log_error===true){
            @mkdir($this->error_path);
            ini_set("log_errors",1);
            ini_set("error_log", $this->error_path);
        }
    }

    public function screen_error(){
        if($this->screen_error===true){
            ini_set('display_errors',1);
        }else{
            ini_set('display_errors',0);
        }
    }
    public function log_error(){
        error_log($this->error);
    }

    public function compile_log(){
        $this->create_log();
        $this->log_error();
        $this->screen_error();
    }


}