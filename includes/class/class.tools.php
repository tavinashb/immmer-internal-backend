<?php

/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 5/10/16
 * Time: 9:12 AM
 */

if(!defined('INCLUDE_SCRIPT')) die('You do not have permission to access this!');

class tools
{


    public function httpStatus($statusCode,$definition=null){

        ob_start();
        header('HTTP/1.1 '.$statusCode.' '.$definition);
        return $statusCode;
    }

    public function url(){
        if(isset($_REQUEST['first'])){
            $urlparam=explode(DIRECTORY_SEPARATOR,$_REQUEST['first']);
            /* @@@@ url parameters @@@@@*/

            unset($urlparam[0]);
            $mainUrl=array();

            foreach($urlparam as $url){
                $mainUrl[]=$url ;
        }
           return $mainUrl;

        }

    }


}