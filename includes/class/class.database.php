<?php

/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 4/1/16
 * Time: 10:41 PM
 */


class database
{

    private $host = HOST;
    private $user = DBUSER;
    private $dbname = DBNAME;
    private $pass = DBPASS;
    private $conn;
    private $database_schema=DATABASE_SCHEMA;

    public function __construct()
    {//this constructs new database connect
        try {
        $this->conn = new PDO("mysql:dbname=$this->dbname;host=$this->host;", $this->user, $this->pass);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
        $this->conn->exec('SET NAMES utf8');
    } catch (PDOException $e) {
die('could not connect to database');
}
    }
    public function sanitizer($data, $deep=false){//turn deep on for trim, strip_tags and stripslashes
        if($deep==true){
            $data=trim($data);
            $data=stripslashes($data);
            $data=strip_tags($data);
        }
        return htmlspecialchars($data);
    }

    public function disconnect()
    {//disconnects database connection
        return $this->conn = false;
    }

    public function doquery($sql, $arr)
    {//prepares all database queries, thisi
        $stmt = $this->conn->prepare($sql);
        $stmt->execute($arr);
        return $stmt;
    }


    public function subs($st_field, $start = 0, $end = '-1')
    {//go back one level by default
        return substr($st_field, $start, $end);
    }

    public function innerInsert($table, $data = array(), $field = array())
    {//insert data to the table specified
        $datacount = count($data);
        $fieldcount = count($field);
        if ($datacount == $fieldcount) {
            $st_field = '';
            $st_data = array();
            $question_tag = '';
            foreach (array_combine($field, $data) as $col => $dt) {
                $st_field .= "`" . $col . "`,";
                $st_data[] = $dt;
                $question_tag .= '?,';
            }
            $question_tags = $this->subs($question_tag);
            $st_fields = $this->subs($st_field);

            try {
                $insert = $this->doquery("INSERT INTO `$table`($st_fields)values($question_tags)", array_values($st_data));
            } catch (PDOException $e) {
                return false;
            }
            if ($insert) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function innerupdate($table, $data = array(), $field = array(), $wildcard, $wildcardvalue = array(), $wildcard2 = null, $wildcardvalue2 = array())
    {//updates data to the table specified
        //wildcard can be one or two depending, the last parameter is open to sql injection, do not use it with user input
        $datacount = count($data);
        $fieldcount = count($field);
        if ($datacount == $fieldcount) {
            $st_field = '';
            $st_data = array();

            foreach (array_combine($field, $data) as $col => $dta) {
                $st_field .= "`" . $col . "`= ?,";
                $st_data[] = $dta;
            }
            $str_fields = $this->subs($st_field);
            if ($wildcard2 == null) {
                $secondwild = null;
                $arr_data = array_merge_recursive(array_values($st_data), array_values(array($wildcardvalue)));
            } else {
                $secondwild = 'and `' . $wildcard2 . '` =?';
                $arr_data = array_merge(array_values($st_data), array_values(array($wildcardvalue)), array_values(array($wildcardvalue2)));
            }
            try {

                //var_dump("UPDATE `$table` set ".$str_fields." WHERE `$wildcard` = ? " . $secondwild, array_values($arr_data));
                $update = $this->doquery("UPDATE `$table` set ".$str_fields." WHERE `$wildcard` = ? " . $secondwild, $arr_data);
            } catch (PDOException $e) {
                return false;
            }
            if ($update) {

                return true;
            } else {echo 'hey';
                return false;
            }
        }
    }

    public function delete($table, $wildcard, $wildcardvalue = array(), $wildcard2 = null, $wildcardvalue2 = array())
    {//all function can only have two match method, not more than 2
        //delete data
        if ($wildcard2 == null) {
            $secondwild = null;
            $arr_data = array_merge(array($wildcardvalue));
        } else {
            $secondwild = 'and `' . $wildcard2 . '` =?';
            $arr_data = array_merge(array($wildcardvalue), array($wildcardvalue2));
        }
        try {
            $del = $this->doquery("DELETE FROM `$table` where `$wildcard` = ?".$secondwild, $arr_data);
        } catch (PDOException $e) {
            return false;
        }
        if ($del) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchMultiple($table,$selectvalues,$wildcard=null,$wildcardvalue=array(),$wildcard2=null,$wildcardvalue2=array(),$lastcondition=null)
    {//fetch multiple values as array out of the database
        //lastcondtion is not prepared so do not use with foreign input as this might introduce sql injection into the application
        if ($wildcard == null) {
            $firstwild = null;
            $arr_data = null;
        } else {
            $firstwild = 'where`' . $wildcard . '` =?';
            $arr_data = array($wildcardvalue);
        }
        if ($wildcard2 == null) {
            $secondwild = null;
            $arr_data = array_merge(array($wildcardvalue));
        } else {
            $secondwild = 'and `' . $wildcard2 . '` =?';
            $arr_data = array_merge(array($wildcardvalue), array($wildcardvalue2));
        }
        if($lastcondition==null){
            $lastcondition=null;
        }

        try{

            $stmt=$this->doquery("SELECT ".$selectvalues." FROM  `$table` ".$firstwild." ".$secondwild." ".$lastcondition,$arr_data);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);//use foreach and value you want to return
        }catch(PDOException $e){
            return false;
        }
    }

    public function fetchSearch($table,$selectvalues,$col,$like)
    {//fetch multiple values as array out of the database
//this is solely for search

        try{
            $stmt=$this->doquery("SELECT ".$selectvalues." FROM  `$table` where `$col` like ?",array($like.'%'));
            return $stmt->fetchAll();//use foreach and value you want to return
        }catch(PDOException $e){
            return false;
        }
    }
    public function SearchCount($table,$selectvalues,$col,$like)
    {//fetch multiple values as array out of the database
//this is solely for search counting

        try{
            $stmt=$this->doquery("SELECT count(`$selectvalues`) FROM  `$table` where `$col` like ?",array($like.'%'));
            foreach($stmt->fetchAll() as $rowcount){
                $data=$rowcount[0];
            }//use foreach and value you want to return
            return $data;
        }catch(PDOException $e){
            return false;
        }
    }
    public function fetchSingle($table,$selectvalue,$wildcard=null,$wildcardvalue=array(),$wildcard2=null,$wildcardvalue2=array(),$lastcondition=null)
    {//fetch single data out of the specified table
        //lastcondition variable is not prepared, pls do not use with foreign input because it might introduce an sql injection to the system
        if ($wildcard == null) {
            $firstwild = null;
            $arr_data = null;
        } else {
            $firstwild = 'where`' . $wildcard . '` =?';
            $arr_data = array($wildcardvalue);
        }
        if ($wildcard2 == null) {
            $secondwild = null;
            $arr_data = array_merge(array($wildcardvalue));
        } else {
            $secondwild = 'and `' . $wildcard2 . '` =?';
            $arr_data = array_merge(array($wildcardvalue), array($wildcardvalue2));
        }
        if($lastcondition==null){
            $lastcondition=null;
        }

        try{
            $stmt=$this->doquery("SELECT `$selectvalue` FROM  `$table` ".$firstwild." ".$secondwild." ".$lastcondition,$arr_data);
            return $stmt->fetch()[$selectvalue];//nothing else needed, returns the data
        }catch(PDOException $e){
            return false;
        }
    }

    public function fetch($table,$selectvalues,$wildcard=null,$wildcardvalue=array(),$wildcard2=null,$wildcardvalue2=array(),$lastcondition=null)
    {//fetch single data out of the specified table
        //lastcondition variable is not prepared, pls do not use with foreign input because it might introduce an sql injection to the system
        if ($wildcard == null) {
            $firstwild = null;
            $arr_data = null;
        } else {
            $firstwild = 'where`' . $wildcard . '` =?';
            $arr_data = array($wildcardvalue);
        }
        if ($wildcard2 == null) {
            $secondwild = null;
            $arr_data = array_merge(array($wildcardvalue));
        } else {
            $secondwild = 'and `' . $wildcard2 . '` =?';
            $arr_data = array_merge(array($wildcardvalue), array($wildcardvalue2));
        }
        if($lastcondition==null){
            $lastcondition=null;
        }

        try{
            $stmt=$this->doquery("SELECT ".$selectvalues." FROM  `$table` ".$firstwild." ".$secondwild." ".$lastcondition,$arr_data);
            return $stmt->fetch(PDO::FETCH_ASSOC);//nothing else needed, returns the data
        }catch(PDOException $e){
            return false;
        }
    }

    public function countTable($table,$valtocount,$wildcard=null,$wildcardvalue=array(),$wildcard2=null,$wildcardvalue2=array(),$lastcondition=null)
    { //this counts table
        if ($wildcard == null) {
            $firstwild = null;
            $arr_data = null;
        } else {
            $firstwild = 'where`' . $wildcard . '` =?';
            $arr_data = array($wildcardvalue);
        }
        if ($wildcard2 == null) {
            $secondwild = null;
            $arr_data = array_merge(array($wildcardvalue));
        } else {
            $secondwild = 'and `' . $wildcard2 . '` =?';
            $arr_data = array_merge(array($wildcardvalue), array($wildcardvalue2));
        }
        if($lastcondition==null){
            $lastcondition=null;
        }
        try{
            $val=$this->sanitizer($valtocount);
            $stmt=$this->doquery("SELECT COUNT($val) FROM  `$table` ".$firstwild." ".$secondwild,$arr_data);
            $dd='';
            foreach($stmt->fetchAll() as $countdd){
                $dd=$countdd[0];
            }
            return $dd;
        }catch(PDOException $e){
            return false;
        }

    }


}