<?php

/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 5/15/16
 * Time: 4:09 PM
 */

class latlong
{
    private $googleapi=GOOGLE_API_ADDRESS;
    private $apikey=GOOGLE_API_KEY;
    public $unit;
    private $output;
    public $addressFrom;
    public $addressTo;



    public function decodedData($address){
        //$geocodeFrom = file_get_contents($this->googleapi.$address.'&'.$this->apikey);
       // return json_decode($geocodeFrom);
        $ch=$this->googleapi.$address.'&'.$this->apikey;
        $sh=curl_init();
        curl_setopt($sh,CURLOPT_URL,$ch);
        curl_setopt($sh,CURLOPT_RETURNTRANSFER,true);
        $curl_rule=curl_exec($sh);
        curl_close($sh);
               return  json_decode($curl_rule);

    }

    public function unitinstaller(){
        if ($this->unit == "K") {
            return ceil($this->output * 1.609344).' km';
        } else if ($this->unit == "N") {
            return ceil($this->output * 0.8684).' nm';
        } else {
            return ceil($this->output).' mi';
        }
    }

    public function getDistance(){
        //Change address format
        $unit=$this->unit;
        $addressFrom=$this->addressFrom;
        $addressTo=$this->addressTo;
        $formattedAddrFrom = str_replace(' ','+',$addressFrom);
        $formattedAddrTo = str_replace(' ','+',$addressTo);

        //Send request and receive json data

        $outputTo = $this->decodedData($formattedAddrTo);
        $outputFrom =$this->decodedData($formattedAddrFrom);
            //Get latitude and longitude from geo data
        $latitudeFrom = $outputFrom->results[0]->geometry->location->lat;
        $longitudeFrom = $outputFrom->results[0]->geometry->location->lng;
        $latitudeTo = $outputTo->results[0]->geometry->location->lat;
        $longitudeTo = $outputTo->results[0]->geometry->location->lng;

        //Calculate distance from latitude and longitude
        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        $this->unit=$unit;
        $this->output=$miles;
          return  $this->unitinstaller();

    }
}


