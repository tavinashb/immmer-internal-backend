<?php

/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 5/10/16
 * Time: 8:57 AM
 */
class user extends database
{

    public $request_method;
    public $tool_class;
    public $sqlSparam;
    public $sqlSInj;
    public $json;
    public $request;
    public $oauth;
    public $access_token;
    public $dbfield=array();
    public $dbdata=array();
    public $long;




    public function __construct()
    {
        parent::__construct();
        $this->request_method=$_SERVER['REQUEST_METHOD'];
        $this->tool_class=new tools();
        $this->json=new jsonCoder();
        $this->long=new latlong();
        $this->request=$_REQUEST;
    }

    private function viewuserInfo(){
        $user_query = $this->doquery("SELECT
 `user_id`,`user_firstname`,`user_lastname`,`user_email`,`profile_photo`,
 `user_nickname`,`user_current_location`,`user_subscription`,`user_permission_id`,
 `user_last_login`,`user_social`, `user_slogan`,`user_sex`,
  `country_loc_name`,`state_loc_name`,`subscr_name`,`subscr_datetime`,`subscri_expired`
 FROM   `users` a INNER JOIN `location` b on a.user_location=b.country_loc_id
INNER JOIN subscription e on a.user_subscription=e.subscr_id where a.user_account_status=1 " . $this->sqlSparam, $this->sqlSInj);

        foreach ($user_query->fetchAll(PDO::FETCH_ASSOC) as $user_data) {
            $feed_data = $this->doquery(
                "select DISTINCT `feed_id`,`feed_pricing`,`video_scrshot`,`feed_title`,`feed_location`,`feed_type`,`feed_start_datetime`,`feed_end_datetime`,`feed_text` from `feed`
        where feed_user_id=? and `feed_start_datetime` > NOW()", array($user_data['user_id']));
            $feedinfarray = array();

            $feedcount = 0;
            foreach ($feed_data->fetchAll(PDO::FETCH_ASSOC) AS $feedin) {

                $this->long->unit='K';
                $this->long->addressTo=$user_data['user_current_location'];
                $this->long->addressFrom=$feedin['feed_location'];

                $feedinfarray[] = array(
                    'feed_title' => $feedin['feed_title'],
                    'feed_location' => $feedin['feed_location'],
                    'feed_type' => $feedin['feed_type'],
                    'feed_start'=>$feedin['feed_start_datetime'],
                    'feed_end'=>$feedin['feed_end_datetime'],
                    'feed_text' => $feedin['feed_text'],
                    'feed_price'=>$feedin['feed_pricing'],
                    'feed_scrshot'=>$feedin['video_scrshot'],
                    'feed_distance'=>$this->long->getDistance(),
                    'feed_likes'=>$this->countTable('feed_interaction','feed_int_id','feed_id',$feedin['feed_id']),
                    'feed_file' => $this->fetchMultiple('file', "`file_path`,`file_type`", 'file_feed_id', $feedin['feed_id'])
                );
                $feedcount++;

            }

            $shout=$this->doquery("select `shout_id`,`shout_description`,`shout_title`,`shout_feed_id`,`file_path`
            from `feed_shout` a INNER JOIN `file` b on b.file_id=a.shout_file_id where a.shout_user_id=?",array($user_data['user_id']));


           foreach($this->doquery("select sum(`feed_int_id`) from `feed_interaction` a INNER JOIN `feed` b on a.feed_id=b.feed_id WHERE b.feed_user_id=?",array($user_data['user_id']))->fetchAll() as $feedlikesdata){
               $all_likes=$feedlikesdata[0];
            }
            $userjson = array(
                'id' => $user_data['user_id'],
                'firstname' => $user_data['user_firstname'],
                'lastname' => $user_data['user_lastname'],
                'email' => $user_data['user_email'],
                'username' => $user_data['user_nickname'],
                'current_location' => $user_data['user_current_location'],
                'permission' => $this->fetchSingle('permission', 'permission_access_level', 'permission_id', $user_data['user_permission_id']),
                'last_login' => $user_data['user_last_login'],
                'socialpages' => $this->fetchMultiple('social', "*", 'social_id', $user_data['user_social']),
                'slogan' => $user_data['user_slogan'],
                'all_likes'=>$all_likes,
                'sex' => $user_data['user_sex'],
                'from_country' => $user_data['country_loc_name'],
                'from_state' => $user_data['state_loc_name'],
                'subscribed_plan' => $user_data['subscr_name'],
                'subscription_date' => $user_data['subscr_datetime'],
                'subscription_expired' => $user_data['subscri_expired'],
                'likes' => $this->countTable('user_interaction', 'user_int_id', 'user_int_type', 'likes', 'user_int_to', $user_data['user_id']),
                'followers' => $this->countTable('user_interaction', 'user_int_id', 'user_int_type', 'follow', 'user_int_to', $user_data['user_id']),
                'following' => $this->countTable('user_interaction', 'user_int_id', 'user_int_type', 'follow', 'user_int_from', $user_data['user_id']),
                'friends' => $this->countTable('user_relationship', 'relationship_id', 'relationship_type', 'friends', 'relationship_to', $user_data['user_id']),
                'events' => $feedinfarray,
                'shout'=>$shout->fetchAll(PDO::FETCH_ASSOC),
                'feed_count'=>$feedcount,
                'profile_picture' => $user_data['profile_photo'],
                'interests' => $this->fetchMultiple('interests', "`interest_name`", 'interest_user_id', $user_data['user_id']),
                'status'=>'success'
            );

        }
        $this->tool_class->httpStatus(200);
        return $userjson;
}

    public function ifIsset($data,$field){
        if(isset($data)){
            $this->dbdata[]=htmlspecialchars($data);
            $this->dbfield[]=$field;
        }
    }


    public function interests($table,$useridcol,$type,$name=array()){
        $this->delete(
            $table,
            $useridcol,
            $this->userId()
        );
        $field=array();
            $data=array();
        $date=date('Y-m-d H:i:s');
        $insert='';
        if($table==='user_category'){

            foreach($name as $catname){
                $insert.=$this->innerInsert($table,
                    array(htmlspecialchars($this->userId()),htmlspecialchars($catname),$date,'shopping'),
                    array('category_user_id','user_category_name','user_category_datetime_added','user_category_type')
                );
            }

        }elseif($table==='interests'){

            foreach($name as $interestname){
                $insert.=$this->innerInsert($table,
                    array(htmlspecialchars($this->userId()),htmlspecialchars($interestname),$date),
                    array('interest_user_id',
                        'interest_name',
                        'interest_date'
                    )
                );
            }
        }

        if($insert){
            $this->tool_class->httpStatus(200);
            return successJson('Account successfully Updated',$this->tool_class->httpStatus(201));
        }

    }


    public function imageuploader(){
        $base=$this->request['photo'];

                    try{
                        $image=str_replace('data:image/gif;base64,','',$base);
                        $ext='.gif';
                    }catch(Exception $e){
                        return false;
                    }

        $uploaddir='./img/'.$ext;
        $img=base64_decode(str_replace(' ','+',$image));
        $done=file_put_contents($uploaddir,$img);
        if($done){
            return $uploaddir;
        }else{
            return false;
        }

    }


    public function userId(){
        return $this->fetchSingle('access_token', 'user_id', 'access_token', $this->access_token);
    }



    private function editUser()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $updateuser = $this->request;

            if (isset($updateuser['photo'])) {
                if ($this->imageuploader() == false) {
                    return errorJson('Either file extension is not valid or file is corrupted, contact admin', $this->tool_class->httpStatus(501));

                } else {
                    $image = $_SERVER['HTTP_HOST'] . '/' . $this->imageuploader();
                }
            } else {
                $image = null;
            }



            $loopUsrdata = array(
                'user_firstname' => explode(' ', $updateuser['name'])[0],
                'user_lastname' => explode(' ', $updateuser['name'])[1],
                'user_nickname' => $updateuser['nickname'],
                'user_slogan' => $updateuser['slogan'],
                'user_about_me' => $updateuser['about'],
                'user_email' => $updateuser['email'],
                'user_mobile' => $updateuser['mobile'],
                'profile_photo' => $image
            );


            //      $form_file=$_FILES['photo'];
            //      $ext=mime_content_type($form_file['name']);
            //      $extension=end(explode('.',$form_file['photo']['name']));
            //      switch($ext){
            //          case 'image/png':
            //              $go=true;
            //              break;
            //          case 'image/jpeg':
            //              $go=true;
            //              break;
            //          case  'image/gif':
            //              $go=true;
            //              break;
            //          case  'image/bmp':
            //              $go=true;
            //              break;
            //          default:
            //              $go=false;
            //
            //           }
            //
            //      if($go==false){
            //          return errorJson('File type not allowed',$this->tool_class->httpStatus(400));
            //      }else{
            //
            //
            //      $ifmoved=move_uploaded_file($form_file['tmp_name'],'./img/'.$this->userId().'.'.$extension);
            //     if($ifmoved){
            //
            //         $imagurl=$_SERVER['HTTP_HOST'].'/img/'.$this->userId().'.'.$extension;
            //         $this->innerupdate('users',array($imagurl),array('profile_photo'),'user_id',$this->userId());
            //
            //     }else{
            //         return errorJson('Unable to upload file, contact admin',$this->tool_class->httpStatus(501));
            //     }
            //  }
            foreach ($loopUsrdata as $key => $usrdata) {
                if($usrdata!=null){
                    $this->ifIsset($key, $usrdata);

                }
            }
            $updupdate = $this->innerupdate(
                'users',
                $this->dbfield,
                $this->dbdata,
                'user_id',
                $this->userId()
            );
            if(isset($updateuser)){
                $updupdate .= $this->interests('interests', 'interest_user_id', null, $updateuser['interests']);

            }
            if(isset($updateuser)){
                $updupdate .= $this->interests('user_category', 'category_user_id', 'user_category_name', $updateuser['shopping']);

            }


            if ($updupdate) {
                return successJson('Account successfully updated', 201);

            }


        }
    }

    private function interest(){
      $interest=$this->fetchMultiple('interests','interest_name','interest_user_id',$this->userId());

        if($interest){
            $this->tool_class->httpStatus(200);
            return $interest;
        }
    }
    private function deleteusr(){
        if($this->fetchSingle('users','user_account_status','user_id',$this->userId())===1){
            $created=$this->innerupdate(
                'users',
                array(4),
                array('user_account_status'),
                'user_id',
                $this->userid()
            );
            if($created){
                return successJson('Account successfully deleted',$this->tool_class->httpStatus(201));

            }

        }
     }

    public function compileUsers(){
        /* @@@@@@@@@@@@@@@@ user api @@@@@@@@@@@@@@@@*/

        $oauth = new oauth('access_token', $_SERVER['REQUEST_METHOD']);
            if($this->tool_class->url()[1]==='view'){
                $utransid =$this->userId();

                if ($this->tool_class->url()[2] === $utransid) {
                    $this->sqlSparam = " and user_id=?";
                    $this->sqlSInj[] = $this->tool_class->url()[2];
                } elseif (empty($this->tool_class->url()[2]) && $this->tool_class->url()[1] === 'view') {
                    $this->sqlSparam = " and user_id=?";
                    $this->sqlSInj[] = $utransid;
                } else {
                    $oauth->malformed = 'error';
                }
                if($oauth->malformed!=='error'){
                $rawusrdata=$this->viewuserInfo();
                }else{
                    $rawusrdata= array(errorJson('Permission denied', $this->tool_class->httpStatus(400, null)));
                }
            }elseif($this->tool_class->url()[1]==='update'){
                $rawusrdata=$this->editUser();
            }elseif($this->tool_class->url()[1]==='delete'){
                $rawusrdata=$this->deleteusr();
            }elseif($this->tool_class->url()[1]==='interest'){
                $rawusrdata=$this->interest();
            }

            return $rawusrdata;


    }




}