<?php

/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 5/10/16
 * Time: 9:35 AM
 */
if(!defined('INCLUDE_SCRIPT')) die('You do not have permission to access this!');
class events extends database
{
    public $sqlSuffix;
    public $sqlInjSuffix=array();
    public $request_method;
    public $tool_class;
    public $json;
    public $post;
    public $get;
    public $request;
    public $access_token;




    public function __construct()
    {
        parent::__construct();
        $this->request_method=$_SERVER['REQUEST_METHOD'];
        $this->tool_class=new tools();
        $this->json=new jsonCoder();
        $this->post=$_POST;
        $this->get=$_GET;
        $this->request=$_REQUEST;
    }
    private function viewEvent(){
        $stmt=$this->doquery("select `feed_id`, `feed_user_id`,`feed_title`,
`feed_location`,`feed_text`,`feed_type`,`feed_start_datetime`,`feed_pricing`,
`feed_end_datetime`,`feed_privacy`,`feed_access`,`feed_description`,
`feed_tag` FROM `feed` ".$this->sqlSuffix,$this->sqlInjSuffix);
        $live_stream_data=array();

        foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $feed_source){
            $shout_sql=$this->doquery("SELECT `shout_user_id`,`shout_time`,`file_path`, `file_type`,
`file_datetime_added` as dateadded,`shout_description`,`shout_file_id`,
`shout_title`,`shout_feed_id` FROM `feed_shout` a
INNER JOIN `file` b on b.file_id=a.shout_file_id",null);

            $fr_owners=$this->doquery("SELECT `user_nickname`AS friends from `users` a
INNER JOIN `user_relationship` b on b.relationship_from=a.user_id
WHERE b.relationship_to=?",array($feed_source['feed_user_id']));
            $event_hosts=$this->doquery("SELECT `user_nickname` as `event_host`,`user_id` as host_id from `event_hosts` a INNER JOIN
`users` b on b.user_id=a.event_hosts_userid WHERE `event_hosts_feed_id`=?",array($feed_source['feed_id']));

            $live_stream_data[]=array(
                'attendant'=>$this->countTable('feed_attendants','attendance_id',
                    'attendance_feed_id',$feed_source['feed_id']),

                'feed_likes'=>$this->countTable('feed_interaction','feed_int_id',
                    'feed_id',$feed_source['feed_id'],'feed_int_type','likes'),

                'feed_follow'=>$this->countTable('feed_interaction','feed_int_id',
                    'feed_id',$feed_source['feed_id'],'feed_int_type','follow'),


                'feed_shares'=>$this->countTable('feed_interaction','feed_int_id',
                    'feed_id',$feed_source['feed_id'],'feed_int_type','share'),

                'feed_shouts'=>$shout_sql->fetchAll(PDO::FETCH_ASSOC),

                'feed_view'=>$this->countTable('feed_view','view_id','view_feed_id',
                    $feed_source['feed_id']),

                'feed_video'=>$this->fetchMultiple('file',"`file_path`,`file_type`",
                    'file_feed_id',$feed_source['feed_id'],'file_type','VIDEO'),

                'feed_audio'=>$this->fetchMultiple('file',"`file_path`,`file_type`",
                    'file_feed_id',$feed_source['feed_id'],'file_type','AUDIO'),

                'feed_picture'=>$this->fetchMultiple('file',"`file_path`,`file_type`",
                    'file_feed_id',$feed_source['feed_id'],'file_type','PICTURE'),

                'feed_location'=>$this->fetchSingle('location','country_loc_name',
                    'country_loc_id',$feed_source['feed_location']),

                'owners_likes'=>$this->countTable('user_interaction','user_int_id',
                    'user_int_to',$feed_source['feed_user_id'],'user_int_type','likes'),

                'owners_follow'=>$this->countTable('user_interaction','user_int_id',
                    'user_int_to',$feed_source['feed_user_id'],'user_int_type','follow'),

                'owner_friends'=>$fr_owners->fetchAll(PDO::FETCH_ASSOC),
                'event_hosts'=>$event_hosts->fetchAll(PDO::FETCH_ASSOC),

                'category'=>$this->fetchMultiple('categories','category_name','category_feed_id',$feed_source['feed_id']),

                'feed_pricing'=>($feed_source['feed_access']==='free'?null:'$'.$feed_source['feed_pricing']),
                'feed_type'=>$feed_source['feed_type'],
                'feed_start'=>$feed_source['feed_start_datetime'],
                'feed_end'=>$feed_source['feed_end_date_time'],
                'feed_access'=>$feed_source['feed_access'],
                'feed_description'=>$feed_source['feed_description'],
                'feed_owner'=>$feed_source['feed_user_id']

            );

            $live_stream_data[]=array(
                'status'=>'success',
                'status_code'=>$this->tool_class->httpStatus(200,null)
            );

        }
        return $live_stream_data;
    }

    private function eventInjector(){
        $sqlInjSuffix = array();
        if (isset($this->tool_class->url()[1])) {
            $event = $this->tool_class->url()[1];
        }else{
            $event = 'events';
        }


        $this->sqlSuffix = ' WHERE `feed_type`=? ';
        $this->sqlInjSuffix[] = $event;
        if (isset($this->tool_class->url()[2])) {
            if ($this->tool_class->url()[2] === 'now') {
                $t = '1 HOUR';
            } else {
                $t = '7 DAY';
            }
            $this->sqlSuffix .= ' and `feed_end_datetime`
 BETWEEN NOW() - INTERVAL ' . $t . ' AND NOW() ';
        }

        if (isset($this->tool_class->url()[3])) {
            $loc_id = $this->fetchSingle
            ('location', 'country_loc_id', 'country_loc_name',
                $this->sanitizer($this->tool_class->url()[3]));
            if (!empty($loc_id)) {
                $this->sqlSuffix .= " and `feed_location`=? ";
                $this->sqlInjSuffix[] = $loc_id;
            }
        }

        if (isset($this->tool_class->url()[4])) {
            $usr = $this->sanitizer($this->tool_class->url()[4]);
            $ans=$this->sanitizer($this->tool_class->url()[5]);
            $usr = $this->fetchSingle('users', 'user_id', 'user_nickname', $ans);
            if ($usr==='user') {
                $this->sqlSuffix .= " and `feed_user_id`=? ";
                $this->sqlInjSuffix[] = $usr;
            }else{
                $this->sqlSuffix .= " and `feed_id`=? ";
                $this->sqlInjSuffix[] = $ans;
            }
        }
    }
    private function editEvent(){

    }

    private function deteteEvent(){

    }

    public function compileEvent(){

        if($this->tool_class->url()[1]==='delete'){

        }elseif($this->tool_class->url()[1]==='update'){

        }else{
            $this->eventInjector();
            return $this->viewEvent();
        }
    }
}





