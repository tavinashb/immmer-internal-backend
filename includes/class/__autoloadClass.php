<?php
/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 4/1/16
 * Time: 11:26 PM
 */

require DOCUMENT_ROOT.'/includes/class/class.log.php';
$log=new log();
$log->compile_log();


require DOCUMENT_ROOT.'/includes/class/class.database.php';
require DOCUMENT_ROOT.'/includes/class/class.jsonCoder.php';
require DOCUMENT_ROOT.'/includes/class/class.tools.php';
require DOCUMENT_ROOT.'/includes/class/class.oauth.php';
require DOCUMENT_ROOT.'/includes/class/class.response.php';
require DOCUMENT_ROOT.'/includes/class/class.request.php';
require DOCUMENT_ROOT.'/includes/class/class.mailer.php';
require DOCUMENT_ROOT.'/includes/class/class.user.php';
require DOCUMENT_ROOT.'/includes/class/class.events.php';
require DOCUMENT_ROOT.'/includes/class/class.latlong.php';
