<?php
/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 5/10/16
 * Time: 10:21 AM
 */

$db = new database();
$json = new jsonCoder();
$tools=new tools();
$cevent=new events();
$cusr=new user();
$long=new latlong();
$authorization=@$_SERVER['HTTP_AUTHORIZATION'];
$access_token=end(explode(' ',$authorization));
$oauth = new oauth('access_token', $_SERVER['REQUEST_METHOD']);
function errorJson($description, $code)
{
    return array('details' => null,
        'description' =>$description,
        'status_code' => $code,
        'status'=>'error'
    );
}

function successJson($description, $code)
{
    return array('details' => null,
        'description' =>$description,
        'status_code' => $code,
        'status'=>'success'
    );
}
