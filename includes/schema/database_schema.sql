-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 10, 2016 at 08:11 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `immmer_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--
CREATE DATABASE IF NOT EXISTS immmer_api;
USE DATABASE immmer_api;



CREATE TABLE IF NOT EXISTS `access_token` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(10) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_token`
--

INSERT INTO `access_token` (`access_token`, `client_id`, `user_id`, `expires`) VALUES
('a1d03303d41c19734662880a966f9ac7', 'b5c01', 1, '2016-04-28 14:26:04'),
('760b0697707cbf081ba7b590f415c430', 'ec02f', 1, '2016-05-07 11:26:04'),
('ea33a498bde9b71735b9dbc94496c7e7', '570e0', 1, '2016-05-07 11:26:27'),
('b9a34c32a311cfb13c0cb6f2dcb86931', '667ef', 1, '2016-05-07 11:28:52'),
('0f898ea271e3816cbd299a9b3f660ad2', '2def1', 1, '2016-05-07 11:29:33'),
('fb5f3e639ba4304be0fe766db968e0a1', '2bbe3', 1, '2016-05-07 11:37:01'),
('3d42ac24a9057b323d4953e8beb1e98c', '5d68e', 1, '2016-05-07 12:04:36'),
('1dc43c88196f06e93885dac11dc32a9a', '5e452', 1, '2016-05-07 16:25:35'),
('07f2f148ad4ff3d9dcfa9f808aaa9461', '0b342', 1, '2016-05-10 21:26:07');

-- --------------------------------------------------------

--
-- Table structure for table `account_status`
--

CREATE TABLE IF NOT EXISTS `account_status` (
  `status_id` int(2) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(10) NOT NULL,
  `status_datetime` datetime NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `account_status`
--

INSERT INTO `account_status` (`status_id`, `status_name`, `status_datetime`) VALUES
(1, 'active', '2016-04-28 06:00:00'),
(2, 'pending', '2016-04-28 00:00:00'),
(3, 'suspended', '2016-04-28 00:00:00'),
(4, 'deleted', '2016-04-28 05:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_name` varchar(20) NOT NULL,
  `category_added_time` datetime NOT NULL,
  `category_feed_id` int(255) NOT NULL,
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_name`, `category_added_time`, `category_feed_id`, `category_id`) VALUES
('hear', '2016-04-28 00:00:00', 1, 1),
('see', '2016-04-28 00:00:00', 1, 2),
('entertainment', '2016-04-28 00:00:00', 1, 3),
('do', '2016-04-28 00:00:00', 1, 4),
('taste', '2016-04-28 00:00:00', 1, 5),
('learn', '2016-04-28 00:00:00', 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `event_hosts`
--

CREATE TABLE IF NOT EXISTS `event_hosts` (
  `event_hosts_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `event_hosts_userid` int(11) NOT NULL,
  `event_hosts_feed_id` bigint(255) NOT NULL,
  PRIMARY KEY (`event_hosts_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `event_hosts`
--

INSERT INTO `event_hosts` (`event_hosts_id`, `event_hosts_userid`, `event_hosts_feed_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `feed`
--

CREATE TABLE IF NOT EXISTS `feed` (
  `feed_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `feed_user_id` int(11) DEFAULT NULL,
  `feed_title` varchar(64) NOT NULL,
  `feed_location` bigint(255) NOT NULL,
  `feed_text` text NOT NULL,
  `feed_type` enum('shout','happening','events') NOT NULL,
  `feed_start_datetime` datetime NOT NULL,
  `feed_end_datetime` datetime NOT NULL,
  `feed_privacy` enum('private','public') NOT NULL,
  `feed_access` enum('paid','free','donation') NOT NULL,
  `feed_pricing` int(20) NOT NULL,
  `feed_description` text NOT NULL,
  `feed_tag` int(11) DEFAULT NULL,
  PRIMARY KEY (`feed_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `feed`
--

INSERT INTO `feed` (`feed_id`, `feed_user_id`, `feed_title`, `feed_location`, `feed_text`, `feed_type`, `feed_start_datetime`, `feed_end_datetime`, `feed_privacy`, `feed_access`, `feed_pricing`, `feed_description`, `feed_tag`) VALUES
(1, 1, 'Dummy live channel', 1, 'just for testing the mobile application, and making sure things works as required. Hello Immmer', 'events', '2016-05-10 20:17:00', '2016-05-09 09:33:00', 'public', 'donation', 50, 'this feed is happening now for entrepreneur. wanna join?', 3),
(2, 1, 'just another dummy ', 1, 'this is just another version of dummy event api. just trying to check the services', 'events', '2016-05-10 19:41:00', '2016-05-09 09:30:00', 'public', 'paid', 100, 'anything can work here', 6);

-- --------------------------------------------------------

--
-- Table structure for table `feed_attendants`
--

CREATE TABLE IF NOT EXISTS `feed_attendants` (
  `attendance_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `attendance_user_id` int(11) NOT NULL,
  `attendance_feed_id` bigint(255) NOT NULL,
  PRIMARY KEY (`attendance_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `feed_attendants`
--

INSERT INTO `feed_attendants` (`attendance_id`, `attendance_user_id`, `attendance_feed_id`) VALUES
(1, 2, 1),
(2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `feed_interaction`
--

CREATE TABLE IF NOT EXISTS `feed_interaction` (
  `feed_int_id` int(255) NOT NULL AUTO_INCREMENT,
  `int_from` int(11) NOT NULL,
  `feed_id` int(11) NOT NULL,
  `feed_int_type` enum('likes','follow','share') NOT NULL,
  `feed_int_datetime` datetime NOT NULL,
  PRIMARY KEY (`feed_int_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `feed_interaction`
--

INSERT INTO `feed_interaction` (`feed_int_id`, `int_from`, `feed_id`, `feed_int_type`, `feed_int_datetime`) VALUES
(1, 3, 1, 'likes', '2016-04-28 00:00:00'),
(2, 2, 1, 'share', '2016-04-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `feed_shout`
--

CREATE TABLE IF NOT EXISTS `feed_shout` (
  `shout_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `shout_user_id` int(11) NOT NULL,
  `shout_time` datetime NOT NULL,
  `shout_description` text NOT NULL,
  `shout_file_id` bigint(255) NOT NULL,
  `shout_title` varchar(64) NOT NULL,
  `shout_feed_id` bigint(255) NOT NULL,
  PRIMARY KEY (`shout_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `feed_shout`
--

INSERT INTO `feed_shout` (`shout_id`, `shout_user_id`, `shout_time`, `shout_description`, `shout_file_id`, `shout_title`, `shout_feed_id`) VALUES
(1, 2, '2016-04-28 00:00:00', 'This is fun', 2, 'See this', 1),
(2, 3, '2016-04-28 11:00:00', 'This is not fun', 2, 'Couldn''t ve come', 1);

-- --------------------------------------------------------

--
-- Table structure for table `feed_tag`
--

CREATE TABLE IF NOT EXISTS `feed_tag` (
  `feed_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_tagger_user` int(11) NOT NULL,
  `feed_tagged_user` int(11) NOT NULL,
  `feed_tag_feed_id` bigint(255) NOT NULL,
  PRIMARY KEY (`feed_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feed_view`
--

CREATE TABLE IF NOT EXISTS `feed_view` (
  `view_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `view_user_id` int(11) NOT NULL,
  `view_feed_id` int(11) NOT NULL,
  `view_datetime` datetime NOT NULL,
  PRIMARY KEY (`view_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `feed_view`
--

INSERT INTO `feed_view` (`view_id`, `view_user_id`, `view_feed_id`, `view_datetime`) VALUES
(1, 1, 1, '2016-04-28 00:00:00'),
(2, 2, 1, '2016-04-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `file_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `file_path` text NOT NULL,
  `file_type` enum('video','audio','file','picture') NOT NULL,
  `file_datetime_added` datetime NOT NULL,
  `file_feed_id` bigint(255) NOT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_path`, `file_type`, `file_datetime_added`, `file_feed_id`) VALUES
(1, 'http://localhost/projects/immmer-backend/videos/1.mp4', 'video', '2016-04-28 00:00:00', 1),
(2, 'http://localhost/projects/immmer_backend/shout/testing.jpg', 'picture', '2016-04-28 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

CREATE TABLE IF NOT EXISTS `interests` (
  `interest_id` int(11) NOT NULL AUTO_INCREMENT,
  `interest_name` varchar(64) NOT NULL,
  `interest_date` datetime NOT NULL,
  `interest_user_id` int(11) NOT NULL,
  PRIMARY KEY (`interest_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=210 ;

--
-- Dumping data for table `interests`
--

INSERT INTO `interests` (`interest_id`, `interest_name`, `interest_date`, `interest_user_id`) VALUES
(207, 'eat', '2016-05-10 19:14:50', 1),
(208, 'go', '2016-05-10 19:14:50', 1),
(209, 'come', '2016-05-10 19:14:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `country_loc_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `country_loc_name` varchar(100) NOT NULL,
  `state_loc_name` varchar(100) NOT NULL,
  PRIMARY KEY (`country_loc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`country_loc_id`, `country_loc_name`, `state_loc_name`) VALUES
(1, 'America', 'New york'),
(2, 'Nigeria', 'lagos');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempt`
--

CREATE TABLE IF NOT EXISTS `login_attempt` (
  `attempt_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `attempt_user_id` int(11) NOT NULL,
  `attempt_datetime` datetime NOT NULL,
  `attempt_security_token` varchar(50) NOT NULL,
  `attempt_status` enum('success','failed') NOT NULL,
  `attempt_ip_addess` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`attempt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_access_level` varchar(30) NOT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permission_id`, `permission_access_level`) VALUES
(1, 'users'),
(2, 'admin'),
(3, 'superadmin');

-- --------------------------------------------------------

--
-- Table structure for table `refresh_token`
--

CREATE TABLE IF NOT EXISTS `refresh_token` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(10) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `refresh_token`
--

INSERT INTO `refresh_token` (`refresh_token`, `client_id`, `user_id`, `expires`) VALUES
('c131441712fdcf3ab58435cd326ca567', 'b5c01', 1, '2016-05-22 00:13:28'),
('fb2deee97ad4c44b02669cfb00696651', 'ec02f', 1, '2016-05-31 11:16:04'),
('41e0d1630ec635946608ac66c54c7e7d', '570e0', 1, '2016-05-31 11:16:27'),
('d68c88f126ac0c2d4ff2b378394a0847', '667ef', 1, '2016-05-31 11:18:52'),
('1fbc7220192ccf9c3d7ca2c3618ef8b4', '2def1', 1, '2016-05-31 11:19:33'),
('c5e3f50480a5d95972e905a939adc5d3', '2bbe3', 1, '2016-05-31 11:25:21'),
('d6146d495f4a361e13a3f190fbeaa77f', '5d68e', 1, '2016-05-31 11:54:36'),
('8cba3189f22aaf422c1885dc600e50b2', '5e452', 1, '2016-05-31 16:15:35'),
('f4587ee33997b3557474a82e4bd4b7a9', '0b342', 1, '2016-05-31 16:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE IF NOT EXISTS `social` (
  `social_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `social_facebook` varchar(100) NOT NULL,
  `social_twitter` varchar(100) NOT NULL,
  `social_linkdin` varchar(100) NOT NULL,
  `social_eventbrite` varchar(100) NOT NULL,
  PRIMARY KEY (`social_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `subscr_id` int(1) NOT NULL AUTO_INCREMENT,
  `subscr_name` enum('bronze','silver','gold','platin') NOT NULL,
  `subscr_datetime` datetime NOT NULL,
  `subscri_expired` datetime NOT NULL,
  PRIMARY KEY (`subscr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`subscr_id`, `subscr_name`, `subscr_datetime`, `subscri_expired`) VALUES
(1, 'bronze', '2016-05-09 00:00:00', '2016-06-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(20) NOT NULL,
  `user_lastname` varchar(20) NOT NULL,
  `user_email` varchar(320) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_location` bigint(255) DEFAULT NULL,
  `user_nickname` varchar(64) NOT NULL,
  `user_current_location` bigint(255) DEFAULT NULL,
  `user_subscription` int(2) NOT NULL,
  `user_permission_id` int(2) NOT NULL,
  `user_date_joined` datetime NOT NULL,
  `user_last_login` int(20) DEFAULT NULL,
  `user_account_status` int(1) NOT NULL,
  `user_social` int(11) DEFAULT NULL,
  `user_slogan` text NOT NULL,
  `user_sex` enum('male','female') NOT NULL,
  `user_v_code` varchar(50) NOT NULL,
  `profile_photo` text,
  `user_about_me` text,
  `user_mobile` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_firstname`, `user_lastname`, `user_email`, `user_password`, `user_location`, `user_nickname`, `user_current_location`, `user_subscription`, `user_permission_id`, `user_date_joined`, `user_last_login`, `user_account_status`, `user_social`, `user_slogan`, `user_sex`, `user_v_code`, `profile_photo`, `user_about_me`, `user_mobile`) VALUES
(1, 'joseph', 'minor', 'martinomartin_5@hotmail.com', '$P$BXTjWVq5IoUlYu0.caNa.XNJ15a2.d/', 1, 'joseph minor', NULL, 1, 1, '2016-04-28 00:44:16', NULL, 1, NULL, 'start living', 'male', '536c732fefb6ad8876208a41f31abc54', NULL, 'martin chuka', '74683408'),
(3, 'test', 'malaki', 'mcmailmartinrenew@yahoo.com', '$P$B7DYnD3/KY9UtaB0XIbJYlYJEE8RVl0', NULL, 'Yasha Khandelwal', NULL, 1, 1, '2016-04-28 05:54:05', NULL, 1, NULL, '', 'male', 'b8563fc8059ba382a7fabf9f04a6297e', NULL, NULL, NULL),
(6, 'martin', 'chuka', 'martinomartin_5@hotmail.com', '$P$BluELugicWhUmNYyTdpp.8jZAccG9d0', NULL, 'Yasha Khandelwal2', NULL, 1, 1, '2016-05-07 12:03:51', NULL, 1, NULL, '', 'male', 'c4e698f56bfa151e4f164ce014b36fce', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_category`
--

CREATE TABLE IF NOT EXISTS `user_category` (
  `user_category_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `category_user_id` int(11) NOT NULL,
  `user_category_type` enum('shopping','theme') NOT NULL,
  `user_category_name` varchar(200) NOT NULL,
  `user_category_datetime_added` datetime NOT NULL,
  PRIMARY KEY (`user_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=196 ;

--
-- Dumping data for table `user_category`
--

INSERT INTO `user_category` (`user_category_id`, `category_user_id`, `user_category_type`, `user_category_name`, `user_category_datetime_added`) VALUES
(193, 1, 'shopping', 'walmart', '2016-05-10 19:14:51'),
(194, 1, 'shopping', 'shoprite', '2016-05-10 19:14:51'),
(195, 1, 'shopping', 'ebay', '2016-05-10 19:14:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_interaction`
--

CREATE TABLE IF NOT EXISTS `user_interaction` (
  `user_int_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `user_int_from` int(11) NOT NULL,
  `user_int_to` int(11) NOT NULL,
  `user_int_type` enum('likes','follow') NOT NULL,
  `user_int_datetime` datetime NOT NULL,
  PRIMARY KEY (`user_int_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_interaction`
--

INSERT INTO `user_interaction` (`user_int_id`, `user_int_from`, `user_int_to`, `user_int_type`, `user_int_datetime`) VALUES
(1, 2, 1, 'follow', '2016-04-28 00:00:00'),
(2, 3, 1, 'likes', '2016-04-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_relationship`
--

CREATE TABLE IF NOT EXISTS `user_relationship` (
  `relationship_id` int(11) NOT NULL AUTO_INCREMENT,
  `relationship_to` int(11) NOT NULL,
  `relationship_from` int(11) NOT NULL,
  `relationship_type` enum('friends','married','fiancee','in_relationship') DEFAULT NULL,
  `relationship_datetime` datetime NOT NULL,
  PRIMARY KEY (`relationship_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_relationship`
--

INSERT INTO `user_relationship` (`relationship_id`, `relationship_to`, `relationship_from`, `relationship_type`, `relationship_datetime`) VALUES
(1, 1, 2, 'fiancee', '2016-04-28 00:00:00'),
(2, 1, 3, 'friends', '2016-05-09 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;