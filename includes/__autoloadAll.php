<?php
/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 4/1/16
 * Time: 11:51 PM
 */
require DOCUMENT_ROOT.'/includes/__header.php';
require DOCUMENT_ROOT.'/includes/__config.php';
require DOCUMENT_ROOT.'/includes/url_param.php';
require DOCUMENT_ROOT.'/includes/error_values.php';
require DOCUMENT_ROOT.'/includes/libraries/__autoloadlib.php';
require DOCUMENT_ROOT.'/includes/class/__autoloadClass.php';
require DOCUMENT_ROOT.'/includes/__def_class.php';
