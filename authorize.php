<?php
/**
 * Created by IntelliJ IDEA.
 * User: Yasha Khandelwal
 * Date: 4/2/16
 * Time: 12:45 PM
 */
(defined('__DIR__'))? define('DOCUMENT_ROOT','__DIR__'): define('DOCUMENT_ROOT',dirname(__FILE__));

require DOCUMENT_ROOT.'/__autoload.php';
$grant=@$_SERVER['HTTP_AUTHORIZATION'];
$oauth=new oauth(htmlspecialchars($grant),@$_SERVER['REQUEST_METHOD']);

switch(htmlspecialchars($grant)){
    case 'facebook_login_grant';
        $oauth->password_auth(@$_REQUEST);
        break;
    case 'refresh_token';
        $oauth->refresh_token(@$_REQUEST);
        break;
    case 'facebook_registration_grant';
        $oauth->registration_auth(@$_REQUEST);
        break;
}

